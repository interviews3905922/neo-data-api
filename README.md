# NEO Data API

## Overview
This project implements an API to retrieve and store Near-Earth Object (NEO) data from the NASA NeoWs API. The application pulls NEO data from the NASA API, converts it to the desired format, and stores it in a SQLite database. Additionally, it exposes an API endpoint to retrieve the stored NEO data.

## Features
- Fetches NEO data from NASA NeoWs API
- Stores NEO data in a SQLite database
- Converts epoch timestamps to ISO8601 timestamps (GMT)
- Converts metric measurements to imperial measurements (feet, miles)
- Treats `neo_reference_id` as personally identifiable information (PII) and stores only the last 4 digits

## Requirements
- Python 3.x
- Flask
- SQLite

## Installation
1. Clone the repository: `git clone https://gitlab.com/interviews3905922/neo-data-api.git`
2. Install the required dependencies: `pip install -r requirements.txt`

## Configuration
1. Obtain an API key from the NASA API: [https://api.nasa.gov/](https://api.nasa.gov/)
2. Open the `config.py` file and replace `"YOUR_NASA_API_KEY"` with your actual API key

## Usage
1. Run the application: `flask --app main run `
2. The application will fetch NEO data from the NASA API, convert and store it in the database.
3. Fetch and store NEO data from NASA: `http://localhost:5000/fetch-neos`
3. Access the NEO data from database API endpoint: `http://localhost:5000/neos`

## API Endpoint
- `/neos`: Retrieves the stored NEO data from database in JSON format
- `/fetch-neos`: Retrieves NEO data from NASA API and stores in database

## Project Structure
- `main.py`: Main entry point of the application
- `config.py`: Configuration file for API keys and settings
- `database.py`: Database setup and data storage functions
- `api.py`: Flask web server and API endpoint definitions
- `utils.py`: Utility functions for data transformation and conversion

## Testing
1. Run the unit tests: `python -m unittest discover tests`

## Contributing
1. Fork the repository
2. Create your feature branch: `git checkout -b feature-name`
3. Commit your changes: `git commit -am 'Add new feature'`
4. Push to the branch: `git push origin feature-name`
5. Submit a pull request

## License
[MIT License](LICENSE)
