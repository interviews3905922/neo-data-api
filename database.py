import sqlite3
from config import DB_FILE
import json


def create_tables():
    """
    Create necessary tables in the SQLite database if they don't exist.

    :return: None
    """
    conn = sqlite3.connect(DB_FILE)
    c = conn.cursor()
    schema = '''CREATE TABLE IF NOT EXISTS neo_data (
                    neo_reference_id TEXT,
                    name TEXT,
                    close_approach_date TEXT,
                    miss_distance_feet REAL,
                    miss_distance_miles REAL,
                    is_potentially_hazardous BOOLEAN
                )'''
    c.execute(schema) # Execute the CREATE TABLE query to create the neo_data table if it doesn't exist
    conn.commit()
    conn.close()


def store_neo_data(neo_data):
    """
    Store NEO data in the neo_data table of the SQLite database.

    :param neo_data: Tuple containing NEO data values
    :return: None
    """
    conn = sqlite3.connect(DB_FILE)
    c = conn.cursor()
    c.execute("INSERT INTO neo_data VALUES (?, ?, ?, ?, ?, ?)", neo_data)  # Execute the INSERT query to store the NEO data
    conn.commit()
    conn.close()


def fetch_neo_data():
    """
    Fetch NEO data from the neo_data table and convert it to a JSON format.
    
    :return: JSON data containing NEO data and count
    """
    conn = sqlite3.connect(DB_FILE)
    c = conn.cursor()
    c.execute("SELECT * FROM neo_data")
    rows = c.fetchall()

    # Convert rows to a list of dictionaries
    results = []
    for row in rows:
        result = {}
        for idx, column in enumerate(c.description):
            result[column[0]] = row[idx]
        results.append(result)
    
    # Create a dictionary to hold the final result and count
    json_data = {'neo_data': results, 'neo_count': len(results)}
    
    conn.close()
    return json.dumps(json_data)