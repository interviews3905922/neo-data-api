from flask import Flask, jsonify, request
import requests
from utils import convert_to_imperial, convert_to_iso8601
from database import create_tables, store_neo_data, fetch_neo_data
from config import NASA_API_KEY

app = Flask(__name__)

@app.route("/fetch-neos")
def fetch_neos():
    """
    Fetch NEOs (Near-Earth Objects) from NASA API and store them in the database.

    This function is a route handler for the "/fetch-neos" endpoint.
    :return: JSON response containing the fetched NEO data or an error message
    """
    create_tables()  # Create necessary tables in the database if they don't exist

    start_date = request.args.get('start_date', '1982-12-10')
    end_date = request.args.get('end_date', 'NONE')

    if end_date == 'NONE':
        url = f"https://api.nasa.gov/neo/rest/v1/feed?start_date={start_date}&api_key={NASA_API_KEY}"
    else:
        url = f"https://api.nasa.gov/neo/rest/v1/feed?start_date={start_date}&end_date={end_date}&api_key={NASA_API_KEY}"
    
    response = requests.get(url)

    if response.status_code == 200:
        neo_data = response.json()

        neos = neo_data["near_earth_objects"]

        # Iterate over the NEOs and extract relevant information
        for date in neos:
            for neo in neos[date]:
                neo_reference_id = neo["neo_reference_id"][-4:]
                name = neo["name"]
                close_approach_date = convert_to_iso8601(neo["close_approach_data"][0]["epoch_date_close_approach"])
                miss_distance_miles = float(neo["close_approach_data"][0]["miss_distance"]["miles"])
                miss_distance_feet = convert_to_imperial(miss_distance_miles)
                is_potentially_hazardous = neo["is_potentially_hazardous_asteroid"]

                # Store the NEO data in the database
                store_neo_data((neo_reference_id, name, close_approach_date, miss_distance_feet, miss_distance_miles, is_potentially_hazardous))
        
        return jsonify(neo_data)  # Return the fetched NEO data as JSON response
    else:
        return jsonify({"error": "Failed to fetch NEO data"}), response.status_code  # Return error message if the fetch fails


@app.route("/neos")
def get_neos():
    """
    Retrieve NEO data from the database and return it as a JSON response.

    This function is a route handler for the "/neos" endpoint.
    :return: JSON response containing the retrieved NEO data
    """
    neo_data = fetch_neo_data() # Retrieve NEO data
    return neo_data


@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"