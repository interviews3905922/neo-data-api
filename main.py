from api import app

if __name__ == "__main__":
    """
    Start the Flask application.

    :return: None
    """
    app.run()
