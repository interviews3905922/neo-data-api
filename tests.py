import unittest
from api import app
import json

class APITestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def test_hello_world(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode('utf-8'), '<p>Hello, World!</p>')

    def test_fetch_neos(self):
        response = self.app.get('/fetch-neos')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.data.decode('utf-8'))
        self.assertTrue("near_earth_objects" in data)

    def test_get_neos(self):
        response = self.app.get('/neos')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.data.decode('utf-8'))
        self.assertTrue("neo_data" in data)
        self.assertTrue("neo_count" in data)

if __name__ == '__main__':
    unittest.main()
