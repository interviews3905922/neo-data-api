import datetime

def convert_to_imperial(miles):
    """
    Convert distance in miles to feet.

    :param miles: Distance value in miles
    :return: Converted distance value in feet
    """
    feet = miles * 5280
    return round(feet, 2)


def convert_to_iso8601(epoch):
    """
    Convert epoch timestamp to ISO 8601 format.

    :param epoch: Epoch timestamp
    :return: ISO 8601 formatted timestamp
    """
    datetime_obj = datetime.datetime.utcfromtimestamp(epoch/1000)
    iso8601_timestamp = datetime_obj.strftime('%Y-%m-%dT%H:%M:%SZ')
    return iso8601_timestamp